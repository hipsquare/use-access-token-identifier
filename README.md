# @hipsquare/use-access-code-identifier

A hook that identifies a query parameter called `strapiOidcToken` in browser and Capacitor environments. It seamlessly handles detecting if the user is in a browser or a Capcaitor. For browser apps, it uses the current URL's query parameters. For Capacitor apps, it tries to find launch parameters and parse it from there.

Once the token is identified is is stored in local storage.

## Installation

```sh
yarn add @hipsquare/use-access-code-identifier
```

## Usage

```typescript
import { usestrapiOidcTokenIdentifier } from "@hipsquare/use-access-code-identifier";
const { strapiOidcToken, logout } = usestrapiOidcTokenIdentifier();

useEffect(
  () => console.log("Access token identified: ", strapiOidcToken),
  [strapiOidcToken]
);
```
