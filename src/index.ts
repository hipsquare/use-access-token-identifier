import { App as CapacitorApp } from "@capacitor/app";
import { Browser } from "@capacitor/browser";
import { Capacitor } from "@capacitor/core";
import { useEffect, useState } from "react";

/**
 * Hook that identifies an access token from the URL and sets it in the auth context
 */
export const useAccessTokenIdentifier = () => {
  const [strapiOidcToken, setstrapiOidcToken] = useState<string>();

  useEffect(() => {
    if (strapiOidcToken) {
      localStorage.setItem("strapiOidcToken", strapiOidcToken);
      return;
    }
  }, [strapiOidcToken]);

  useEffect(() => {
    if (localStorage.getItem("strapiOidcToken")) {
      setstrapiOidcToken(localStorage.getItem("strapiOidcToken")!);
    }
  }, []);

  // effect to read access token from URL if it is set
  useEffect(() => {
    async function onAppUrlIdentified(url: string) {
      const searchParams = new URL(url).searchParams;
      const strapiOidcTokenFromUrl = searchParams.get("strapiOidcToken");

      if (
        strapiOidcTokenFromUrl &&
        strapiOidcTokenFromUrl !== strapiOidcToken
      ) {
        setstrapiOidcToken(strapiOidcTokenFromUrl);
        if (Capacitor.isNativePlatform()) {
          try {
            await Browser.close();
          } catch {
            // ignore if browser cannot be closed, as it most likely means there simply isn't a browser open
          }
        }
      }

      // remove query params from URL
      history.pushState({}, "", location.href.split("?")[0]);
    }

    // handle the case that the app is already running and its URL is changed from the outside using an App Link
    CapacitorApp?.addListener("appUrlOpen", ({ url }) => {
      if (!url) {
        return;
      }

      onAppUrlIdentified(url);
    });

    (async () => {
      // fallback: the "normal" browser URL
      let currentUrl = window.location.href;

      // handle the case that the app has been initially opened with a launch URL (and has not been running before)
      if (CapacitorApp) {
        // for capacitor deep links
        const launchUrl = await CapacitorApp.getLaunchUrl();

        if (launchUrl?.url) {
          currentUrl = launchUrl.url;
        }
      }

      onAppUrlIdentified(currentUrl);
    })();
  }, []);

  const handleLogout = () => {
    setstrapiOidcToken("");
    localStorage.removeItem("strapiOidcToken");
  };

  return {
    logout: handleLogout,
    strapiOidcToken,
  };
};
